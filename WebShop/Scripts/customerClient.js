﻿'use strict'

$(function () {
    $.ajax({
        url: window.location.protocol + "//" + window.location.host + "/api/customers"
    }).then(function (data) {        
        fillCustomerRows(data);
    });
});

// - - - CUSTOMERS - - - 

$(".customers").off('click', 'button.remove-btn').on('click', 'button.remove-btn', function () {
    var id = $(this)[0].id;
    var btn = $(this)[0];
    removeCustomer(id, btn);
});

$(".customers").on('click', 'button.calc-amount', function () {
    var id = $(this)[0].id;
    var btn = $(this)[0];
    calcCustomerAmount(id, function (data) {
        $(btn).parent().html(data);
    });
});

function fillCustomerRows(data) {
    $(".customers tbody").html('');
    $(data).each(function (index, item) {
        $(".customers tbody").append("<tr><th>" + item.Id + "</th><th>" + item.Name + '</th><th><button class="btn btn-sm glyphicon glyphicon-remove remove-btn" id="' + item.Id + '"></button></th><th><button class="btn btn-sm glyphicon glyphicon-th-list get-orders" id="' + item.Id + '"></button></th><th><button class="btn btn-sm glyphicon glyphicon-shopping-cart calc-amount" id="' + item.Id + '"></button></th></td>');
    });    
}

function removeCustomer(id, btn) {
    $.ajax({
        url: window.location.protocol + "//" + window.location.host + "/api/customers/" + id,
        type: "DELETE",
        success: function () {
            $(btn).parent().parent().fadeOut();

            $(".orders > table > caption > h3").fadeOut();
            $(".orders > table > caption > h3").html("");
            $(".orders tbody").fadeOut();
            $(".orders tbody").html("");

            $(".products > table > caption > h3").fadeOut();
            $(".products > table > caption > h3").html("");
            $(".products tbody").fadeOut();
            $(".products tbody").html("");
        }
    });
}


function calcCustomerAmount(id, callback) {
    $.ajax({
        url: window.location.protocol + "//" + window.location.host + "/api/customers/" + id + '/calcAmount',
        type: "GET",
        success: function (data) {
            callback(data);
        }
    });
}

// - - - ORDERS - - -

$(".customers").on('click', 'button.get-orders', function(){
    var id = $(this)[0].id;
    var btn = $(this)[0];
    var customerName = $(btn).parent().parent().find("th:eq(1)").html();
    $(".orders > table > caption > h3").html(customerName + " Orders");
    $(".products > table > caption > h3").html("");
    $(".products tbody").html("");
    getOrders(id);
});

$(".orders").off('click', 'button.remove-btn').on('click', 'button.remove-btn', function () {
    var id = $(this)[0].id;
    var btn = $(this)[0];
    removeOrder(id, btn);
});


$(".orders").on('click', 'button.calc-amount', function () {
    var id = $(this)[0].id;
    var btn = $(this)[0];
    calcOrderAmount(id, function (data) {
        $(btn).parent().html(data);
    });
});

function calcOrderAmount(id, callback) {
    $.ajax({
        url: window.location.protocol + "//" + window.location.host + "/api/orders/" + id + '/calcAmount',
        type: "GET",
        success: function (data) {
            callback(data);
        }
    });
}


function getOrders(id) {
    $.ajax({
        url: window.location.protocol + "//" + window.location.host + "/api/orders?customerId=" + id,
        type: "GET",
        success: function (data) {
            fillOrderRows(data);
        }
    });
}

function fillOrderRows(data) {
    $(".orders tbody").html('');
    $(data).each(function (index, item) {
        var rawDate = new Date(item.Date);
        var months = ["January", "February", "March", "April", "May", "June",
            "July", "August", "September", "October", "November", "December"];
        var formatedDate = rawDate.getDay() + ' ' + months[rawDate.getMonth()] + ' ' + rawDate.getFullYear();

        $(".orders tbody").append("<tr><th>" + item.Id + "</th><th>" + formatedDate + '</th><th><button class="btn btn-sm glyphicon glyphicon-remove remove-btn" id="' + item.Id + '"></button></th><th><button class="btn btn-sm glyphicon glyphicon-th-list get-products" id="' + item.Id + '"></button></th><th><button class="btn btn-sm glyphicon glyphicon-shopping-cart calc-amount" id="' + item.Id + '"></button></th></td>');
    });
}

function removeOrder(id, btn) {
    $.ajax({
        url: window.location.protocol + "//" + window.location.host + "/api/orders/" + id,
        type: "DELETE",
        success: function () {
            $(btn).parent().parent().fadeOut();
            $(".products > table > caption > h3").fadeOut();
            $(".products > table > caption > h3").html("");
            $(".products tbody").fadeOut();
            $(".products tbody").html("");
        }
    });
}

// - - - PRODUCTS - - -

$(".orders").on('click', 'button.get-products', function () {
    var id = $(this)[0].id;
    var btn = $(this)[0];
    var orderId = $(btn).parent().parent().find("th:eq(0)").html();
    $(".products > table > caption > h3").html("Order #" + orderId + " Products");
    getProducts(id);
});

$(".products").off('click', 'button.remove-btn').on('click', 'button.remove-btn', function () {
    var id = $(this)[0].id;
    var btn = $(this)[0];
    removeProduct(id, btn);
});


function getProducts(id) {
    $.ajax({
        url: window.location.protocol + "//" + window.location.host + "/api/products?orderId=" + id,
        type: "GET",
        success: function (data) {
            fillProductRows(data);
        }
    });
}

function fillProductRows(data) {
    $(".products tbody").html('');
    $(data).each(function (index, item) {
        $(".products tbody").append("<tr><th>" + item.Id + '</th><th>' + item.Name + '</th><th>' + item.Price + '</th><th><button class="btn btn-sm glyphicon glyphicon-remove remove-btn" id="' + item.Id + '"></th></td>');
    });
}


function removeProduct(id, btn) {
    $.ajax({
        url: window.location.protocol + "//" + window.location.host + "/api/products/" + id,
        type: "DELETE",
        success: function () {
            $(btn).parent().parent().fadeOut();
        }
    });
}