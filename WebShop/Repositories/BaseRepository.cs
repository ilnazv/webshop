﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using WebShop.Interfaces;
using WebShop.Models;

namespace WebShop.Repositories
{
    public class BaseRepository<T> : IRepository<T> where T : class, IEntity
    {
        protected readonly WebShopContext context;

        public BaseRepository(WebShopContext context)
        {
            this.context = context;
        }

        public virtual T Add(T entity)
        {
            context.Set<T>().Add(entity);
            context.SaveChanges();
            return entity;
        }

        public virtual async Task<T> AddAsync(T entity)
        {
            context.Set<T>().Add(entity);
            await context.SaveChangesAsync();
            return entity;
        }

        public virtual long Delete(long id)
        {
            T entityToDelete = context.Set<T>().Find(id);
            context.Set<T>().Remove(entityToDelete);
            return context.SaveChanges();
        }
        
        public virtual async Task<long> DeleteAsync(long id)
        {
            T entityToDelete = await context.Set<T>().FindAsync(id);
            context.Set<T>().Remove(entityToDelete);
            return await context.SaveChangesAsync();
        }

        public virtual T Get(long id)
        {
            return context.Set<T>().Find(id);
        }

        public virtual async Task<T> GetAsync(long id)
        {
            return await context.Set<T>().FindAsync(id);
        }

        public virtual IEnumerable<T> GetAll()
        {
            return context.Set<T>().ToList();
        }

        public virtual async Task<IEnumerable<T>> GetAllAsync()
        {
            return await context.Set<T>().ToListAsync();
        }
    }
}