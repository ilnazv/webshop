namespace WebShop.Migrations
{
    using Models;
    using Repositories;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<WebShop.Models.WebShopContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "WebShop.Models.WebShopContext";
        }

        protected override void Seed(WebShop.Models.WebShopContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            var customersRepo = new BaseRepository<Customer>(context);
            var ordersRepo = new BaseRepository<Order>(context);
            var productsRepo = new BaseRepository<Product>(context);

            var names = new string[] { "John Doe", "Josh Brown", "Ethan Smith", "Michael Johnson", "John Davis", "Jacob Miller", "Tyler Jackson", "Ryan White", "David Harris", "Daniel Anderson" };

            for (int i = 1; i <= 10; i++)
            {
                var newCustomer = new Customer { Id = i, Name = names[i - 1] };
                customersRepo.Add(newCustomer);

                for (int j = 1; j <= 10; j++)
                {
                    var newOrder = new Order { Id = j, Date = DateTime.Now, Customer = newCustomer };
                    ordersRepo.Add(newOrder);

                    for (int k = 1; k <= 10; k++)
                    {
                        var newProduct = new Product { Id = k, Name = $"Product #{k}", Price = k, Order = newOrder };
                        productsRepo.Add(newProduct);
                    }
                }
            }
        }
    }
}
