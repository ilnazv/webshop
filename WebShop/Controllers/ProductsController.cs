﻿using System.Collections.Generic;
using System.Threading.Tasks;
using WebShop.Interfaces;
using WebShop.Models;
using WebShop.Repositories;

namespace WebShop.Controllers
{
    public class ProductsController : BaseController<Product>
    {
        public ProductsController()
        {
            this.repository = new ProductRepository(new WebShopContext());
        }

        public async Task<IEnumerable<Product>> GetByOrderId(long orderId)
        {
            return await ((IProductRepository)repository).GetByOrderIdAsync(orderId);
        }
    }
}
