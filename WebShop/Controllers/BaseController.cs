﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using WebShop.Interfaces;
using WebShop.Models;
using WebShop.Repositories;

namespace WebShop.Controllers
{
    public class BaseController<T> : ApiController where T : class, IEntity
    {
        protected WebShopContext context;

        protected IRepository<T> repository;

        public BaseController()
        {
            context = new WebShopContext();
            repository = new BaseRepository<T>(context);
        }

        // GET: api/Customers
        public virtual async Task<IEnumerable<T>> GetAll()
        {
            return await repository.GetAllAsync();
        }

        // GET: api/Customers/5        
        public virtual async Task<IHttpActionResult> GetById(long id)
        {
            T entity = await repository.GetAsync(id);
            if (entity == null)
            {
                return NotFound();
            }

            return Ok(entity);
        }
        
        // POST: api/Customers        
        public virtual async Task<IHttpActionResult> Post(T entity)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            await repository.AddAsync(entity);            

            return CreatedAtRoute("DefaultApi", new { id = entity.Id }, entity);
        }

        // DELETE: api/Customers/5        
        public virtual async Task<IHttpActionResult> Delete(long id)
        {
            return Ok(await repository.DeleteAsync(id));            
        }
    }
}