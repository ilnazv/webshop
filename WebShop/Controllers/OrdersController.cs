﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using WebShop.Interfaces;
using WebShop.Models;
using WebShop.Repositories;

namespace WebShop.Controllers
{
    public class OrdersController : BaseController<Order>
    {
        public OrdersController()
        {
            this.repository = new OrderRepository(new WebShopContext());
        }

        public async Task<IEnumerable<Order>> GetByCustomerId(long customerId)
        {
            return await ((IOrderRepository)repository).GetByCustomerIdAsync(customerId);
        }

        public override async Task<IHttpActionResult> Delete(long id)
        {
            var productsRepository = new ProductRepository(context);
            
            var products = productsRepository.GetByOrderId(id);

            foreach (var product in products)
            {
                productsRepository.Delete(product.Id);
            }                

            return await base.Delete(id);
        }
        
        [HttpGet]
        [Route("api/orders/{id}/calcAmount")]
        public decimal CalcAmount(long id)
        {            
            var productsRepository = new ProductRepository(context);
            
            return productsRepository.GetByOrderId(id).Sum(x => x.Price); 
        }
    }
}
