﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using WebShop.Models;
using WebShop.Repositories;

namespace WebShop.Controllers
{
    public class CustomersController : BaseController<Customer>
    {
        public override async Task<IHttpActionResult> Delete(long id)
        {
            var ordersRepository = new OrderRepository(context);
            var productsRepository = new ProductRepository(context);

            var orders = ordersRepository.GetByCustomerId(id);

            foreach (var order in orders)
            {
                var products = productsRepository.GetByOrderId(order.Id);

                foreach (var product in products)
                {
                    productsRepository.Delete(product.Id);
                }

                ordersRepository.Delete(order.Id);
            }

            return await base.Delete(id);
        }

        [HttpGet]
        [Route("api/customers/{id}/calcAmount")]
        public decimal CalcAmount(long id)
        {
            decimal amount = 0;

            var ordersRepository = new OrderRepository(context);
            var productsRepository = new ProductRepository(context);

            var orders = ordersRepository.GetByCustomerId(id);

            foreach (var order in orders)
            {
                amount += productsRepository.GetByOrderId(order.Id).Sum(x => x.Price);                
            }

            return amount;
        }
    }
}
