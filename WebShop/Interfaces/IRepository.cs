﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace WebShop.Interfaces
{
    public interface IRepository<T> where T : IEntity
    {
        IEnumerable<T> GetAll();

        Task<IEnumerable<T>> GetAllAsync();

        T Get(long id);

        Task<T> GetAsync(long id);

        T Add(T entity);

        Task<T> AddAsync(T entity);

        long Delete(long id);

        Task<long> DeleteAsync(long id);
    }
}
